package com.borgerstudio.dn.winforms;

import com.borgerstudio.dn.Notifier;
import io.github.autocomplete1.PowerShell;

import java.io.*;

public class WinFormsNotifier implements Notifier {

    private final PowerShell powerShell;

    private final byte[] notifyScript;

    public WinFormsNotifier() throws IOException {
        powerShell = PowerShell.openSession();
        try (InputStream scriptStream = getClass().getResourceAsStream("notify.ps1")) {
            if (scriptStream == null) {
                throw new RuntimeException("Resource com/borgerstudio/dn/winforms/notify.ps1 is missing");
            }
            notifyScript = scriptStream.readAllBytes();
        }
    }

    @Override
    public void send(String appName, String message) {
        try (BufferedReader scriptReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(notifyScript)))) {
            powerShell.executeScript(scriptReader, String.format("-title \"%s\" -text \"%s\"", appName, message));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        if (powerShell != null) {
            powerShell.close();
        }
    }
}
