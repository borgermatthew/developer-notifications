param (
    [string]$title,
    [string]$text
)

[void] [System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms');
$objNotifyIcon=New-Object System.Windows.Forms.NotifyIcon;
$objNotifyIcon.BalloonTipText=$text;
$objNotifyIcon.Icon=[system.drawing.systemicons]::Information;
$objNotifyIcon.BalloonTipTitle=$title;
$objNotifyIcon.BalloonTipIcon='None';
$objNotifyIcon.Visible=$True;
$objNotifyIcon.ShowBalloonTip(5000);
