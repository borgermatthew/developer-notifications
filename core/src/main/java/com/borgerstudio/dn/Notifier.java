package com.borgerstudio.dn;

public interface Notifier extends AutoCloseable {

    void send(String appName, String message);

}
