package com.borgerstudio.dn.freedesktop;

import com.borgerstudio.dn.Notifier;
import org.freedesktop.Notifications;
import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.connections.impl.DBusConnectionBuilder;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.types.UInt32;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;

public class FreeDesktopNotifier implements Notifier, AutoCloseable {

    private final DBusConnection dBusConnection;

    private final Notifications notifications;

    public FreeDesktopNotifier() throws DBusException {
        dBusConnection = DBusConnectionBuilder.forSessionBus().build();
        notifications = dBusConnection.getRemoteObject("org.freedesktop.Notifications", "/org/freedesktop/Notifications", Notifications.class);
    }

    @Override
    public void send(String appName, String message) {
        notifications.Notify(appName, new UInt32(0L), "dialog-warning", appName, message, emptyList(), emptyMap(), 5000);
    }

    @Override
    public void close() throws Exception {
        if (dBusConnection != null) {
            dBusConnection.close();
        }
    }
}
