package org.freedesktop;

import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.messages.DBusSignal;
import org.freedesktop.dbus.types.UInt32;
import org.freedesktop.dbus.types.Variant;

import java.util.List;
import java.util.Map;

/**
 * Auto-generated class.
 */
public interface Notifications extends DBusInterface {


    public GetServerInformationTuple GetServerInformation();
    public void CloseNotification(UInt32 id);
    public UInt32 Notify(String appName, UInt32 replacesId, String appIcon, String summary, String body, List<String> actions, Map<String, Variant<?>> hints, int expireTimeout);
    public List<String> GetCapabilities();


    public static class ActionInvoked extends DBusSignal {

        private final UInt32 arg0;
        private final String arg1;

        public ActionInvoked(String _path, UInt32 _arg0, String _arg1) throws DBusException {
            super(_path, _arg0, _arg1);
            this.arg0 = _arg0;
            this.arg1 = _arg1;
        }


        public UInt32 getArg0() {
            return arg0;
        }

        public String getArg1() {
            return arg1;
        }


    }

    public static class NotificationClosed extends DBusSignal {

        private final UInt32 arg0;
        private final UInt32 arg1;

        public NotificationClosed(String _path, UInt32 _arg0, UInt32 _arg1) throws DBusException {
            super(_path, _arg0, _arg1);
            this.arg0 = _arg0;
            this.arg1 = _arg1;
        }


        public UInt32 getArg0() {
            return arg0;
        }

        public UInt32 getArg1() {
            return arg1;
        }


    }
}