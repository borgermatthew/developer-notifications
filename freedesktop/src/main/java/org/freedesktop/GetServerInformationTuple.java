package org.freedesktop;

import org.freedesktop.dbus.Tuple;
import org.freedesktop.dbus.annotations.Position;

/**
 * Auto-generated class.
 */
public class GetServerInformationTuple extends Tuple {
    @Position(0)
    private String name;
    @Position(1)
    private String vendor;
    @Position(2)
    private String version;
    @Position(3)
    private String specVersion;

    public GetServerInformationTuple(String name, String vendor, String version, String specVersion) {
        this.name = name;
        this.vendor = vendor;
        this.version = version;
        this.specVersion = specVersion;
    }

    public void setName(String arg) {
        name = arg;
    }

    public String getName() {
        return name;
    }
    public void setVendor(String arg) {
        vendor = arg;
    }

    public String getVendor() {
        return vendor;
    }
    public void setVersion(String arg) {
        version = arg;
    }

    public String getVersion() {
        return version;
    }
    public void setSpecVersion(String arg) {
        specVersion = arg;
    }

    public String getSpecVersion() {
        return specVersion;
    }


}