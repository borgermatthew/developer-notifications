package com.borgerstudio.dn.autoconfigure;

import com.borgerstudio.dn.Notifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

public class ApplicationReadyNotification implements ApplicationListener<ApplicationReadyEvent> {

    private final Notifier notifier;

    public ApplicationReadyNotification(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        notifier.send("Spring", "Application is Ready!");
    }

}
