package com.borgerstudio.dn.autoconfigure;

import com.borgerstudio.dn.Notifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ServiceLoader;

@Configuration
@ConditionalOnClass(Notifier.class)
public class DeveloperNotificationsAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public Notifier developerNotifier() {
        return ServiceLoader.load(Notifier.class).findFirst()
                .orElseThrow(() -> new RuntimeException("Could not load a Notifier"));
    }

    @Bean
    public ApplicationReadyNotification applicationReadyNotification(Notifier notifier) {
        return new ApplicationReadyNotification(notifier);
    }

}
