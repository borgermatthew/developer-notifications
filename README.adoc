= Developer Notifications

Abstraction for sending notifications, primarily for development time messages.
This project includes a Spring Boot Starter which emits a notification when the application is ready.
My main use for this is to grab my attention back to the application when I go off to check my email while waiting for it to compile and start.

.Modules
core::
The Notifier interface.
freedesktop::
An popular notification system provided by most Linux environments and Mac.
winforms::
Emits the notification as a WinForms NotifyIcon.
spring-boot-starter::
A starter which sends a helpful message when your Spring Boot application is ready.
spring-boot-example::
An example usage of the notifications starter being used only at development time.